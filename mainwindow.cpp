#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QPixmap img;
    img.load(":/1.bmp");
    ui->label->setPixmap(img);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_2_clicked()
{
    QPixmap img;
    img.load(":/2.bmp");
    ui->label_2->setPixmap(img);
}

void MainWindow::on_pushButton_clicked()
{
    QString fio = ui->lineEdit->text();
    ui->label_3->setText(fio);
}
